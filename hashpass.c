#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

int main(int argc, char * argv[]){
    
    if(argc != 3){
        printf("Invalid number of arguments\n");
        printf("hashpass 'password file' 'output file'\n");
        exit(1);
    }
    
    
    FILE * in = fopen(argv[1], "r");
    if(!in){
        printf("Can't open password file for reading\n");
        exit(1);
    }
    FILE * out = fopen(argv[2], "w");
     if(!out){
        printf("Can't open output file for reading\n");
        exit(1);
    }
    
    char line[1000];
    
    while(fgets(line, 1000, in)){
        line[strlen(line) - 1] = '\0';
        char *hash = md5(line, strlen(line));
        fwrite(hash, sizeof(char), strlen(hash), out);
        fputs("\n", out);
        free(hash);
    }
    
    fclose(in);
    fclose(out);
}